
# Distance package
A distance python package that contains the implementation of L1, L2, DTW, CORT, CORT_DTW and TWED

## Requirement
```
pip install numpy
pip install pandas
pip install cffi==1.10.0
```

## Compiling the C code
To compile the C code, you need to run the following instructions:
```
gcc -c -Wall -Werror -fpic cdtw_mini.c
gcc -shared -o distances.cpython-34m.so cdtw_mini.o
```

You can include ***Utility.py*** to call the distance functions inside your code,
or you can run directly from the command line using ***dist.py***

## Parameters for dist.py
```
-x X        a path to a csv file that contains the sequences
-m M        distance measure (l1, l2, dtw, cort, cort_dtw, twed), default=l2
-w W        window look-ahead for dtw, cort and cort_dtw, default=None
-ax AX      0 for calculating distances between rows, 1 between columns
-o O        distance matrix output file (csv), default=mat.csv
```

### Toy Example

By running the following toy example:
```
python3 distance_toy_example.py
```
You should have the following output:
```
dtw 87.27
dtw_0 177.06
l1 177.06
l2 21.17
cort_dtw 24.64
cort 0.18
twed 461.97
row test 2.6
column test 7.89

Success!
```