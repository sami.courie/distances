import pandas as pd
import argparse
import Utility as Util

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Distances package description:')
    parser.add_argument('-x', type=str, help='file path that contains the sequences (csv)')
    parser.add_argument('-m', type=str, help='distance measure (l1, l2, dtw, cort, cort_dtw, twed)', default='l2')
    parser.add_argument('-w', type=float, help='window look-ahead for dtw, cort and cort_dtw', default=None)
    parser.add_argument('-ax', type=int, help='0 for rows, 1 for columns', default=0)
    parser.add_argument('-o', type=str, help='distance matrix output file (csv)')
    args = parser.parse_args()

    X = pd.read_csv(args.x, header=None).values
    mat = Util.get_distance_matrix(X, measure=args.m, window=args.w, axis=args.ax)

    if args.o:
        pd.DataFrame(mat).to_csv(args.o, index=False, header=False)
    else:
        pd.DataFrame(mat).to_csv('mat.csv', index=False, header=False)
